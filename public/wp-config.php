<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp_dev1' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '^6^|XY2JD^-F+:<%IE vKk?hcw37#CB%+*ZX8ZfG BWUlo#XrY=me%`2=J~?KCM^' );
define( 'SECURE_AUTH_KEY',  '6i<&Z%~smYu;1Ht<}mTqh2`oWj7p*vxgud!*X8c!R4W04pT`@@7JS aZ%Qt`IeM}' );
define( 'LOGGED_IN_KEY',    'PNwXc>~$qF55$u%4P ?YJ9M1saa_w.!` qg}K{Dlaq6hLYTPeQ%G(EG-3DrXns9d' );
define( 'NONCE_KEY',        '1FD4I#]LQBC] En~>w%,xz{X7zT1^~vc?Al73UD^jDy:ERa@m3py6o~=:S#furud' );
define( 'AUTH_SALT',        'QHznI[f<q)[`k2L{^Drh>x~nY_28j^ajnxM4+H}-q9![kP6}a*A`Avl[N%xZxg!f' );
define( 'SECURE_AUTH_SALT', ')s^}@EiQly`x]Xw|M(OvB>yhE[<q>G1@ICt<E3tEdzGs/s2)uLpT?m],^?FDvGqG' );
define( 'LOGGED_IN_SALT',   '<^{Kwn~L2[M/`4OY0C{>{%&Z%+}D1[ryq]a^laO`0G  |CM>:@a<8-(_`0v,h;lK' );
define( 'NONCE_SALT',       '|C %B(3Lukj=osDRYZk9zGgX}]OOvz)VKaCrK+|TG:6}h+0+ZQ>/UAV5+ 3IM}fE' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_dev1_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
